# Running the PHP application with load balancers

## MySQL Database Setup
1. Login to AWS (check you are in the correct role)
2. Go to the search bar and search RDS and click on the service
3. Click create database
4. Engine Option -> MySQL
5. Templates -> Free tier
6. Set username and password
7. Select a security group that is the same as the instance you wish to access this database
8. Click the create database button

## Instance Setup
For this assessment you will need atleast one load balancer and more than one php application running.
### PHP Application
For each php application server you want running, create an instance with the following specifications
* `image` - Amazon Linux 2 AMI (hvm) - Kernel 5.10
* `instance requirements` - 1GB RAM | 1 vCPU 
* `security group` - dynamicdevops-phpapplication | AllowJenkinsMachines

### Load Balancer
For the load balancer create an instance with the following specification 
* `image` - Ubuntu Server 20.04 LTS (HVM)
* `instance requirements` - 1GB RAM | 1 vCPU 
* `security group` - dynamicdevops-phpapplication | AllowJenkinsMachines

## Installing PHP Application with Load Balancer
### Prerequisites
* You have cloned this repository using:

```git clone git@bitbucket.org:garthj98/phpapplication.git```

* Have created the machines listed in `Instance Setup`

* Have created the RDS database listed in `MySQL Database Setup` and have it running. (On this assessment the database-1 RDS can simply be started)

* A running Jenkins server which can run shell code after a push to the repository

### 1. Configure the IP addresses for PHP application servers
Go to the file `ip_php_applications.txt` and paste the private ips of the instances you are trying to install the php application on. Each ip should be on it's own line.

Example:
```
<private_ip_address_for_server_1>
<private_ip_address_for_server_2>
```

### 2. Configure the IP address for the load balancer
Go to the file `ip_load_balancer.txt` and paste the private ip of the instances you are trying to install the loadbalancer on.

Example:
```
<private_ip_address_for_load_balancer>
```

### 3. Configure the dbinfo.inc file
Go to the file `inc/dbinfo.inc` and paste the RDS endpoint from the RDS infomation page and the username and password you set for it. (For this assessment you can use the default values which will give you access to the database-1 RDS)

Example:
```
define('DB_SERVER', '<db-endpoint>');
define('DB_USERNAME', '<db-username>');
define('DB_PASSWORD', '<db-password>');
```

### 4. Make sure you are in the right repository
check that you are in the correct location in your terminal, if not, use the following command:
```
$ cd <path/to/repo>
```
### 5. Push your changes to the repo to initiate a build
```bash
$ git add .
$ git commit -m "Building php application"
$ git push
```

The Jenkins post build shell runs the following command to deploy the application on the servers
```bash
$ ./install_php_application.sh <path/to/key>
```

To access the page which uses php, go to:
```
http://<loadbalancer-ip>/SamplePage.php
```
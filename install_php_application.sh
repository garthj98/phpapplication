# Checks if key has been passed as option
if [ $# -lt 1 ]
then
    echo "Expected at least 1 argument, $# was given instead"
    echo "Expected format: ./install_and_connect.sh <path/to/key>"
    exit 1
fi
key=$1

# reads ips from text file
filename=ip_php_applications.txt
while IFS= read -r ip || [ -n "$ip" ];do
n=$((n+1))
echo "installing onto ip $ip"
# Installs required packages
./bashscript_install_mysql.sh $key $ip
# Moves required files into the correct directory
./bashscript_launch_website.sh $key $ip $n
done < $filename

echo "Finished installing php application"

./haproxy.sh $key

# This is the variable that will be called to log into the ubuntu server
hostname=$1

# This sets a counter in order to check how many arguments have been inputted
counter=0
for arg in $*
do 
counter=$(expr $counter + 1) 
done


# Encourages the user to only input one argument: their IP address
if ((counter<1))
then 
 echo "Please try again with the I.P. address"
 exit 1
 elif ((counter>1))
 then
 echo "Too many arguments inputted."
 exit 1
 else 
 echo "Ready to install nginx."
 fi 


# Logs into the ubuntu server and removes the option for fingerprint
ssh -o StrictHostKeyChecking=no -i ~/.ssh/ch9_shared.pem ubuntu@$hostname '

# Checks to see if nginx is installed
 if sudo nginx -v ;
 then
 echo "Looks like nginx is already installed!"
 else 

 # Updates the system
 sudo apt update

 # Installs nginx
 sudo apt install nginx -y 

 # Small message to inform the user
 echo "Woo! Nginx is installed."

 # Starts nginx
 sudo systemctl start nginx 

 # Sends a html file
 scp -i ~/.ssh/ch9_shared.pem index.nginx-debian.html ubuntu@$hostname:/home/ubuntu/

 # Moves the file to the correct place
 sudo mv index.nginx-debian.html /var/www/html/

 # Restarts/refreshes so that the new html file will be displayed
 sudo systemctl restart nginx
  fi 
' 






# /Users/Amalia/Code/Week_5
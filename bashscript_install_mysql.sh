# Checks if key and ip address has been passed as arguments
if [ $# -lt 2 ]
then
    echo "Expected at least 2 argument, $# was given instead"
    echo "Expected format: ./basgscript_install_mysql.sh <path/to/key> <ip>"
    exit 1
fi
key=$1
ip=$2

# Installs mariadb and php client
if ! output=$(ssh -n -o StrictHostKeyChecking=no -i $key ec2-user@$ip sudo httpd -v)
then
    echo "httpd could not be found"
    ssh -o StrictHostKeyChecking=no -i $key ec2-user@$ip << EOF
    sudo yum update -y
    sudo yum install mariadb-server -y
    sudo systemctl enable mariadb
    sudo systemctl start mariadb
    sudo yum install httpd -y
    sudo yum install amazon-linux-extras -y 
    sudo timeout 60 amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
EOF
fi
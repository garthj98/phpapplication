if [ $# -lt 3 ]
then
    echo "Expected at least 3 argument, $# was given instead"
    echo "Expected format: ./basgscript_install_mysql.sh <path/to/key> <ip>"
    exit 1
fi

key=$1
ip=$2
n=$3
serverName="Power Walking Rules $n"

# Copies required files to targeted instance
scp -i $key -r inc ec2-user@$ip:
scp -i $key SamplePage.php ec2-user@$ip:

# Moves files in correct directory and relaunches Apache
ssh -o StrictHostKeyChecking=no -i $key ec2-user@$ip << EOF
sudo sed 's/Sample page/$serverName/' ~/SamplePage.php > ~/temp.php
sudo cat ~/temp.php > ~/SamplePage.php 
sudo mv ~/SamplePage.php /var/www/html/SamplePage.php
sudo mv inc/ /var/www/inc/
sudo service httpd start
EOF
# Checks if key has been passed as an argument
if [ $# -lt 1 ]
then
    echo "Expected at least 1 argument, $# was given instead"
    echo "Expected format: ./haproxy.sh <path/to/key>"
    exit 1
fi
key=$1
read -r load_balancer < ip_load_balancer.txt

# Reads private ips of servers from text file
filename=ip_php_applications.txt
configlines=""
while IFS= read -r ip || [ -n "$ip" ]; do
    #Creates lines to be added to HAProxy's config file
    n=$((n+1))
    configlines="${configlines}server node$n $ip:80 check
    "
done < $filename

# installs haproxy on our load balancer if it isn't already installed
if ! output=$(ssh -o StrictHostKeyChecking=no -i $key ubuntu@$load_balancer sudo haproxy -v)
then
    echo "haproxy could not be found. will install now"
    ssh -o StrictHostKeyChecking=no -i $key ubuntu@$load_balancer '
    sudo apt update -y
    sudo apt install haproxy -y
    '
fi

# sets the config file
ssh -o StrictHostKeyChecking=no -i $key ubuntu@$load_balancer << EOF
sudo bash -c 'echo "
defaults
        log     global
        mode    http
        option  httplog
        option  dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000
        errorfile 400 /etc/haproxy/errors/400.http
        errorfile 403 /etc/haproxy/errors/403.http
        errorfile 408 /etc/haproxy/errors/408.http
        errorfile 500 /etc/haproxy/errors/500.http
        errorfile 502 /etc/haproxy/errors/502.http
        errorfile 503 /etc/haproxy/errors/503.http
        errorfile 504 /etc/haproxy/errors/504.http

frontend haproxynode
    bind *:80
    mode http
    default_backend backendnodes

backend backendnodes
    balance roundrobin
    option forwardfor
    $configlines
" > /etc/haproxy/haproxy.cfg'

sudo systemctl restart haproxy
EOF

echo "finished installing load balancer"